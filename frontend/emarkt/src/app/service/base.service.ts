import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment.development';

@Injectable({
  providedIn: 'root',
})
export class BaseService<T extends { _id?: string }> {
  entity: string = '';

  constructor(protected http: HttpClient) {}

  getAll(): Observable<T[]> {
    return this.http.get<T[]>(`${environment.apiUrl}/${this.entity}`);
  }

  get(_id: string): Observable<T> {
    return this.http.get<T>(`${environment.apiUrl}/${this.entity}/${_id}`);
  }

  create(entity: T): Observable<T> {
    return this.http.post<T>(`${environment.apiUrl}/${this.entity}`, entity);
  }

  update(entity: T): Observable<T> {
    return this.http.put<T>(
      `${environment.apiUrl}/${this.entity}/${entity._id}`,
      entity
    );
  }

  remove(_id: string): Observable<T> {
    return this.http.delete<T>(`${environment.apiUrl}/${this.entity}/${_id}`);
  }
}
