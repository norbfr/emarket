import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { BehaviorSubject, Observable } from 'rxjs';
import { tap } from 'rxjs/operators';
import { environment } from 'src/environments/environment.development';
import { LoggedInUser } from '../models/logged-in-user.model';
import { UserLogin } from '../models/user-login.model';

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  private readonly BASE_URL: string = environment.apiUrl;
  private readonly userLoggedInObject: BehaviorSubject<any> =
    new BehaviorSubject<any>(null);

  public loginDone: boolean = false;

  constructor(private http: HttpClient) {}

  public login(loginData: UserLogin): Observable<LoggedInUser> {
    return this.http
      .post<LoggedInUser>(this.BASE_URL + '/login', loginData)
      .pipe(
        tap({
          next: (loggedInUser) => {
            if (loggedInUser) {
              this.userLoggedInObject.next({
                name: loggedInUser.name,
                _id: loggedInUser._id,
                permissions: loggedInUser.permissions,
              });

              localStorage.setItem('accessToken', loggedInUser.accessToken);
              localStorage.setItem('refreshToken', loggedInUser.refreshToken);
              localStorage.setItem(
                'user',
                JSON.stringify({
                  name: loggedInUser.name,
                  _id: loggedInUser._id,
                  permissions: loggedInUser.permissions,
                })
              );

              this.loginDone = true;
            }
          },
          error: (err) => {
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            this.userLoggedInObject.next(null);
            console.log(err);

            this.loginDone = true;
          },
          complete: () => {},
        })
      );
  }

  public logout(): Observable<void> {
    const refreshToken = localStorage.getItem('refreshToken');
    return this.http
      .post<void>(this.BASE_URL + '/logout', { refreshToken })
      .pipe(
        tap({
          next: () => {
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('user');
            this.userLoggedInObject.next(null);
          },
          error: (err) => {
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('user');
            this.userLoggedInObject.next(null);
            console.log(err);
          },
          complete: () => {},
        })
      );
  }

  public refreshUserAuthentication(): Observable<any> {
    return this.http
      .post<any>(this.BASE_URL + '/refresh', {
        refreshToken: localStorage.getItem('refreshToken'),
      })
      .pipe(
        tap({
          next: (res) => {
            if (res) {
              localStorage.setItem('accessToken', res.accessToken);
              localStorage.setItem('user', JSON.stringify(res.personData));
              this.userLoggedInObject.next(res.personData);
              this.loginDone = true;
            }
          },
          error: (err) => {
            console.log(err);
            localStorage.removeItem('accessToken');
            localStorage.removeItem('refreshToken');
            localStorage.removeItem('user');
            this.userLoggedInObject.next(null);
            this.loginDone = true;
          },
          complete: () => {},
        })
      );
  }

  public getUserLoggedInObject(): Observable<any> {
    return this.userLoggedInObject.asObservable();
  }

  public get userObjectValue() {
    return JSON.parse(localStorage.getItem('user')!);
  }
}
