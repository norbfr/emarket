export interface LoggedInUser {
  accessToken: string;
  refreshToken: string;
  _id: string;
  name: string;
  email: string;
  permissions: string[];
}
