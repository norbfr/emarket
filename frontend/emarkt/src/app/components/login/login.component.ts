import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/service/auth.service';
import { UserService } from 'src/app/service/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent {
  constructor(
    private fb: FormBuilder,
    private authService: AuthService,
    private userService: UserService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  hide = true;

  loginForm = this.fb.group({
    email: [null, [Validators.required, Validators.email]],
    password: [null, [Validators.required, Validators.minLength(8)]],
  });

  registerForm = this.fb.group({
    name: [null, [Validators.required, Validators.minLength(4)]],
    email: [null, [Validators.required, Validators.email]],
    password: [null, [Validators.required, Validators.minLength(8)]],
  });

  public signIn(form: FormGroup): void {
    if (form.valid) {
      this.authService.login(form.value).subscribe({
        next: () => {},
        error: (err) => {
          console.log(err);
          this._snackBar.open(
            'Sikertelen bejelentkezés! Hibás jelszó vagy felhasználónév.',
            'OK',
            {
              duration: 5000,
              horizontalPosition: 'end',
              verticalPosition: 'top',
            }
          );
        },
        complete: () => {
          form.reset();
          this._snackBar.open('Sikeres bejelentkezés!', 'OK', {
            duration: 5000,
            horizontalPosition: 'end',
            verticalPosition: 'top',
          });
        },
      });
    } else {
      this._snackBar.open(
        'Sikertelen bejelentkezés! Hibás jelszó vagy felhasználónév.',
        'OK',
        {
          duration: 5000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
        }
      );
    }
  }

  public registerUser(form: FormGroup): void {
    this.userService.create(form.value).subscribe({
      next: () => {},
      error: (err) => {
        console.log(err);
        this._snackBar.open('Sikertelen regisztráció!', 'OK', {
          duration: 5000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
        });
      },
      complete: () => {
        this.registerForm.reset();
        this._snackBar.open('Sikeres regisztráció!', 'OK', {
          duration: 5000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
        });
        this.router.navigate(['login']);
      },
    });
  }
}
