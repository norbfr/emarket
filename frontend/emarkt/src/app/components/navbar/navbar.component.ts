import { Component, OnDestroy, OnInit } from '@angular/core';
import { BreakpointObserver, BreakpointState } from '@angular/cdk/layout';
import { Subscription } from 'rxjs';
import { AuthService } from 'src/app/service/auth.service';
import { Router } from '@angular/router';
import { MatSnackBar } from '@angular/material/snack-bar';
import { LoggedInUser } from 'src/app/models/logged-in-user.model';

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss'],
})
export class NavbarComponent implements OnInit, OnDestroy {
  isHandset: boolean = false;

  public userObject?: LoggedInUser;
  public userTokens?: number;
  private userSignInSubscription?: Subscription;
  private userRefreshSubscription?: Subscription;

  constructor(
    private breakpointObserver: BreakpointObserver,
    private authService: AuthService,
    private router: Router,
    private _snackBar: MatSnackBar
  ) {}

  ngOnInit(): void {
    this.breakpointObserver
      .observe(['(max-width: 768px)'])
      .subscribe((state: BreakpointState) => {
        if (state.matches) {
          this.isHandset = true;
        } else {
          this.isHandset = false;
        }
      });

    this.userSignInSubscription = this.authService
      .getUserLoggedInObject()
      .subscribe((user) => (this.userObject = user));

    if (localStorage.getItem('refreshToken')) {
      this.userRefreshSubscription = this.authService
        .refreshUserAuthentication()
        .subscribe();
    } else {
      this.authService.loginDone = true;
    }
  }

  ngOnDestroy(): void {
    if (this.userSignInSubscription) this.userSignInSubscription.unsubscribe();

    if (this.userRefreshSubscription)
      this.userRefreshSubscription.unsubscribe();
  }

  public logout(): void {
    this.authService.logout().subscribe({
      next: () => {},
      error: () => {},
      complete: () => {
        this.router.navigate(['login']);
        this._snackBar.open('Sikeres kijelentkezés!', 'OK', {
          duration: 5000,
          horizontalPosition: 'end',
          verticalPosition: 'top',
        });
      },
    });
  }
}
