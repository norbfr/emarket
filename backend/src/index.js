require('dotenv').config();
const app = require('./server');
const logger = require('./config/logger');
const mongoose = require('mongoose');

const port = process.env.PORT || 3000;

const connectionString = process.env.DB_CONNECTION;
console.log(process.env.PORT);
console.log(connectionString);

mongoose
    .connect(`mongodb+srv://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOST}`)
    .then(() => logger.info('MongoDB connection has been stablished successfully!'))
    .catch((err) => {
        logger.error(err);
        process.exit();
    });

app.listen(port, () => {
    console.log(`App is listening at localhost:${port}`);
});
