const mongoose = require('mongoose');

const ProductSchema = mongoose.Schema({
    name: { type: String, required: true },
    price: { type: Number, required: true },
    description: { type: String, required: true },
    owner: { type: String, required: true },
    dateAdded: { type: String, required: true }
});

module.exports = mongoose.model('Product', ProductSchema);
