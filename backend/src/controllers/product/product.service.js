const Product = require('../../models/product.model');

exports.create = (product) => {
    const newProduct = new Product(product);
    return newProduct.save();
};

exports.findAll = () => Product.find();

exports.findOne = (id) => Product.findById(id);

exports.update = (productId, updatedProduct) => Product.findByIdAndUpdate(productId, updatedProduct, { new: true });

exports.delete = (id) => Product.findByIdAndRemove(id);
