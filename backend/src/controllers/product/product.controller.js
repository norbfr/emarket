const Product = require('../../models/product.model');
const createError = require('http-errors');
const productService = require('./product.service');
const logger = require('../../config/logger');

exports.create = (req, res, next) => {
    const validationErrors = new Product(req.body).validateSync();
    if (validationErrors) {
        return next(new createError.BadRequest(validationErrors));
    };

    const newProduct = {
        ...req.body
    };

    return productService.create(newProduct)
        .then(productData => {
            res.status(201).json(productData);
        })
        .catch(err => {
            console.error(err);
            return next(new createError[500]('Could not saved product'));
        });
};

exports.findAll = (req, res, next) => {
    return productService.findAll().then(productList => {
        res.json(productList);
    })
        .catch(err => {
            console.error(err);
            return next(new createError[500]('Could not send productList'));
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    return productService.findOne(id)
        .then(product => {
            if (!product) return next(new createError.NotFound(`Product not found with id: ${id}`));
            res.json(product);
        })
        .catch((err) => {
            logger.error(err);
            return next(new createError.InternalServerError(err));
        });
};

exports.update = (req, res, next) => {
    const validationErrors = new Product(req.body).validateSync();

    if (validationErrors) {
        return next(new createError.BadRequest(validationErrors));
    };

    const id = req.params.id;

    const updatedProduct = {
        ...req.body
    };

    return productService.update(id, updatedProduct)
        .then(product => {
            res.json(product);
        })
        .catch(err => {
            logger.error(err);
            return next(new createError.BadRequest('Could not updated product with id: ' + id));
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    return productService.delete(id)
        .then(deletedProduct => {
            res.json({
                productDeleted: true,
                id: deletedProduct._id
            });
        })
        .catch(err => {
            logger.error(err);
            return next(new createError[500](`Product could not deleted by id:${id}`));
        });
};
