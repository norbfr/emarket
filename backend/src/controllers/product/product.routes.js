const express = require('express');
const router = express.Router();
const prductController = require('./product.controller');

const authenticationByJWT = require('../../auth/authenticate');

router.get('/', authenticationByJWT, (req, res, next) => {
    return prductController.findAll(req, res, next);
});

router.get('/:id', authenticationByJWT, (req, res, next) => {
    return prductController.findOne(req, res, next);
});

router.post('/', authenticationByJWT, (req, res, next) => {
    return prductController.create(req, res, next);
});

router.put('/:id', authenticationByJWT, (req, res, next) => {
    return prductController.update(req, res, next);
});

router.delete('/:id', authenticationByJWT, (req, res, next) => {
    return prductController.delete(req, res, next);
});

module.exports = router;
