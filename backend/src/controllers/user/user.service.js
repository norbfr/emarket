const User = require('../../models/user.model');

exports.create = (user) => {
    const newUser = user;
    return newUser.save();
};

exports.findAll = () => User.find();

exports.findOne = (id) => User.findById(id);

exports.update = (userId, updatedUser) => User.findByIdAndUpdate(userId, updatedUser, { new: true });

exports.delete = (id) => User.findByIdAndRemove(id);
