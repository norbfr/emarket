const express = require('express');
const router = express.Router();
const userController = require('./user.controller');

const authenticationByJWT = require('../../auth/authenticate');
const adminRoleHandler = require('../../auth/adminOnly');

router.get('/', authenticationByJWT, adminRoleHandler, (req, res, next) => {
    return userController.findAll(req, res, next);
});

router.get('/:id', authenticationByJWT, (req, res, next) => {
    return userController.findOne(req, res, next);
});

router.post('/', (req, res, next) => {
    return userController.create(req, res, next);
});

router.put('/:id', authenticationByJWT, (req, res, next) => {
    return userController.update(req, res, next);
});

router.delete('/:id', authenticationByJWT, adminRoleHandler, (req, res, next) => {
    return userController.delete(req, res, next);
});

module.exports = router;
