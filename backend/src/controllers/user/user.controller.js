const User = require('../../models/user.model');
const createError = require('http-errors');
const userService = require('./user.service');
const logger = require('../../config/logger');

exports.create = (req, res, next) => {
    const validationErrors = new User(req.body).validateSync();
    if (validationErrors) {
        return next(new createError.BadRequest(validationErrors));
    };

    const newUser = new User();

    newUser.name = req.body.name;
    newUser.email = req.body.email;
    newUser.setPassword(req.body.password);

    return userService.create(newUser)
        .then(userData => {
            res.status(201).json(userData);
        })
        .catch(err => {
            console.error(err);
            return next(new createError[500]('Could not saved user'));
        });
};

exports.findAll = (req, res, next) => {
    return userService.findAll().then(userList => {
        res.json(userList);
    })
        .catch(err => {
            console.error(err);
            return next(new createError[500]('Could not send userList'));
        });
};

exports.findOne = (req, res, next) => {
    const id = req.params.id;

    return userService.findOne(id)
        .then(user => {
            if (!user) return next(new createError.NotFound(`User not found with id: ${id}`));
            res.json(user);
        })
        .catch((err) => {
            logger.error(err);
            return next(new createError.InternalServerError(err));
        });
};

exports.update = (req, res, next) => {
    const validationErrors = new User(req.body).validateSync();

    if (validationErrors) {
        return next(new createError.BadRequest(validationErrors));
    };

    const id = req.params.id;

    const updatedUser = {
        ...req.body
    };

    return userService.update(id, updatedUser)
        .then(user => {
            res.json(user);
        })
        .catch(err => {
            logger.error(err);
            return next(new createError.BadRequest('Could not updated user with id: ' + id));
        });
};

exports.delete = (req, res, next) => {
    const id = req.params.id;

    return userService.delete(id)
        .then(deletedUser => {
            res.json({
                userDeleted: true,
                id: deletedUser._id
            });
        })
        .catch(err => {
            logger.error(err);
            return next(new createError[500](`User could not deleted by id:${id}`));
        });
};
