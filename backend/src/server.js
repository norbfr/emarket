const express = require('express');
const morgan = require('morgan');
const logger = require('./config/logger');
const cors = require('cors');

const app = express();

const authHandler = require('./auth/authHandler');

const userRoutes = require('./controllers/user/user.routes');
const productRoutes = require('./controllers/product/product.routes');

app.use(cors());
app.use(express.json());

app.use(morgan('combined', { stream: { write: (message) => logger.info(message) } }));

app.post('/login', authHandler.login);
app.post('/refresh', authHandler.refresh);
app.post('/logout', authHandler.logout);

app.use((req, res, next) => {
    console.log(`HTTP ${req.method} ${req.path}`);
    next();
});

app.use('/users', userRoutes);
app.use('/products', productRoutes);

app.use((err, req, res, next) => {
    logger.error(`ERROR ${err.statusCode}: ${err.message}`);
    res.status(err.statusCode);
    res.json({
        hasError: true,
        message: err.message
    });
});

module.exports = app;
