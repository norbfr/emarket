const jwt = require('jsonwebtoken');

const User = require('../models/user.model');
const Token = require('../models/token.model');

module.exports.login = (req, res) => {
    const { email, password } = req.body;

    User.findOne({ email })
        .then(user => {
            if (user && user.validPassword(password)) {
                const accessToken = jwt.sign({
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    permissions: user.permissions
                }, process.env.ACCESS_TOKEN_SECRET, {
                    expiresIn: process.env.TOKEN_EXPIRY
                });

                const refreshToken = jwt.sign({
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    permissions: user.permissions
                }, process.env.REFRESH_TOKEN_SECRET);

                const newRefreshToken = new Token({
                    refreshToken
                });

                newRefreshToken.save();

                res.json({
                    accessToken,
                    refreshToken,
                    _id: user._id,
                    name: user.name,
                    email: user.email,
                    permissions: user.permissions
                });
            } else {
                res.status(400).json('Name, email or password is incorrect');
            }
        });
};

module.exports.refresh = (req, res) => {
    const { refreshToken } = req.body;

    if (!refreshToken) {
        res.sendStatus(401);
        return;
    }
    Token.findOne({ refreshToken })
        .then(data => {
            if (data) {
                jwt.verify(refreshToken, process.env.REFRESH_TOKEN_SECRET, (err, user) => {
                    if (err) {
                        res.sendStatus(403);
                    }
                    const accessToken = jwt.sign({
                        _id: user._id,
                        name: user.name,
                        email: user.email,
                        permissions: user.permissions
                    }, process.env.ACCESS_TOKEN_SECRET, {
                        expiresIn: process.env.TOKEN_EXPIRY
                    });
                    res.json({
                        accessToken,
                        personData: {
                            _id: user._id,
                            name: user.name,
                            email: user.email,
                            permissions: user.permissions
                        }
                    });
                });
            }
        });
};

module.exports.logout = (req, res) => {
    const { refreshToken } = req.body;
    if (!refreshToken) {
        res.sendStatus(403);
        return;
    }

    Token.findOneAndRemove({ refreshToken })
        .then(data => {
            if (data) {
                res.status(200).send({});
            } else {
                res.sendStatus(403);
            }
        }).catch(err => {
            console.log(err);
            res.status(500).json('Could not logout user');
        });
};
