module.exports = (req, res, next) => {
    if (!req.user.permissions.includes('MANAGE_USER')) {
        return res.sendStatus(401);
    }
    next();
};
